from waveapi import events
from waveapi import model
from waveapi import robot
import logging
import xmlrpc
import xmlrpclib
import base64
from ConfigParser import ConfigParser


# Global initialization
ROBOT_NAME = None 
TRAC_XMLRPC_URL = None
TRAC_USER = None
TRAC_PASS = None
wave_title = None

def OnRobotAdded(properties, context):
  """Invoked when the robot has been added."""
  global wave_title, TRAC_XMLRPC_URL, TRAC_USER, TRAC_PASS
  root_wavelet = context.GetRootWavelet()
  wave_title = root_wavelet.GetTitle()
  blip = context.GetBlipById(root_wavelet.GetRootBlipId())
  transport = xmlrpc.GoogleXMLRPCTransport()
  server = xmlrpclib.ServerProxy("http://" + TRAC_USER + ":" + TRAC_PASS + "@" + TRAC_XMLRPC_URL, transport=transport, verbose=1) 
  if blip.IsRoot():
    area, title = root_wavelet.GetTitle().split(':')
    if(area.lower() == 'wiki'):
      blip.GetDocument().SetText(server.wiki.getPage(title))
      logging.info("Downloaded " + area + " item " + title)
      #root_wavelet.SetTitle(area + ':' + title)       #NotImplementedError()
    if(area.lower() == 'ticket'):
      if title.isdigit():
        id, time_created, time_changed, attributes = server.ticket.get(int(title))
        blip.GetDocument().SetText("Status: %s\nOwner: %s\nType: %s\nSummary: %s\nDescription:\n%s" % (attributes['status'], attributes['owner'], attributes['type'], attributes['summary'], attributes['description'], ))
        logging.info("Downloaded " + area + " item " + title)
#get ticket comments
        ticket_changelog = server.ticket.changeLog(id)
        for time, author, field, oldvalue, newvalue, permanent in ticket_changelog:
          if(field == "comment"):
            root_wavelet.CreateBlip().GetDocument().SetText("Change at %s, by %s\n%s" % (time, author, newvalue))
      else:
        content = blip.GetDocument().GetText()
        id = server.ticket.create(title, content,)
        logging.info("Created new ticket #%s" % id)


def OnBlipSubmitted(properties, context):
  """Invoked on blip edit."""
  global wave_title, TRAC_XMLRPC_URL, TRAC_USER, TRAC_PASS
  logging.info("Blip Submitted")
  blip = context.GetBlipById(properties['blipId'])
  root_wavelet = context.GetRootWavelet()
  if wave_title == None:
    wave_title = root_wavelet.GetTitle()
  if wave_title == root_wavelet.GetTitle():
    area, title = root_wavelet.GetTitle().split(':')
    content = blip.GetDocument().GetText()
    if blip.IsRoot():
      if(area.lower() == 'wiki'):
        transport = xmlrpc.GoogleXMLRPCTransport()
        server = xmlrpclib.ServerProxy("http://" + TRAC_USER + ":" + TRAC_PASS + "@" + TRAC_XMLRPC_URL, transport=transport, verbose=1) 
        server.wiki.putPage(title, content, {"comment": "Updated from wave"})
        logging.info("Updated " + area + " item " + title)
    else:
      if(area.lower() == 'ticket'):
        if title.isdigit():
          server.ticket.update(int(title), contents)
          logging.info("Added comment to ticket #%s" % id)
  else: #title changed, load new page
    OnRobotAdded(properties, context)

def main():
  global wave_title, TRAC_XMLRPC_URL, TRAC_USER, TRAC_PASS
  config = ConfigParser()
  config.read("config.ini")
  ROBOT_NAME = config.get('bot', 'name')
  TRAC_XMLRPC_URL = config.get('bot', 'url')
  TRAC_USER = config.get('bot', 'user')
  TRAC_PASS = config.get('bot', 'pass')
  myRobot = robot.Robot(ROBOT_NAME.capitalize(), 
      image_url='http://' + ROBOT_NAME + '.appspot.com/icon.png',
      version='1',
      profile_url='http://' + ROBOT_NAME + '.appspot.com/')

  myRobot.RegisterHandler(events.WAVELET_SELF_ADDED, OnRobotAdded)
  myRobot.RegisterHandler(events.BLIP_SUBMITTED, OnBlipSubmitted)
  myRobot.Run(debug=True)
  

if __name__ == '__main__':
  main()
